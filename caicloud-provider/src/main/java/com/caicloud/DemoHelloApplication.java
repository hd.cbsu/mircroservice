package com.caicloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoHelloApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoHelloApplication.class, args);
		if(args.length == 2){
			System.out.println(args[0]);
			System.out.println(args[1]);
		}
	}

}
